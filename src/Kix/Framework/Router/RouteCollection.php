<?php
namespace Kix\Framework\Router;

Use Kix\Framework\Router\Route;

/**
 * @author Stepan Anchugov <kixxx1@gmail.com>
 */
class RouteCollection
{

    /**
     * @var Route[]
     */
    private $_routes = array();

    public function addRoute(Route $route)
    {
        if ($route instanceof Route) {
            if ($name = $route->getName()) {
                $this->_routes[$name] = $route;
            } else {
                $this->_routes[] = $route;
            }
        }
    }

    /**
     * @return Route[]
     */
    public function getRoutes()
    {
        return $this->_routes;
    }

}
