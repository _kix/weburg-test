<?php
namespace Kix\Framework\Router;

/**
 * @author Stepan Anchugov <kixxx1@gmail.com>
 */
class Route
{

    /**
     * Regex
     *
     * @var string
     */
    private $_pattern;

    /**
     * e.g. SomeController::someAction
     *
     * @var string
     */
    private $_target;

    /**
     * @var string
     */
    private $_name;

    public function __construct($pattern, $target, $name = '')
    {
        $this->_name = $name;
        $this->_pattern = $pattern;
        $this->_target  = $target;
    }

    public function getName()
    {
        return $this->_name;
    }

    public function getTarget()
    {
        return $this->_target;
    }

    public function splitTarget()
    {
        list($controller, $action) = explode('::', $this->_target);

        return array(
            'controller' => $controller,
            'action'     => $action,
        );
    }

    public function match($uri)
    {
        if (preg_match($this->_pattern, $uri, $matches)) {
            return $matches;
        } else {
            return false;
        }
    }

}
