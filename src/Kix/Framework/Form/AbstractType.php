<?php
namespace Kix\Framework\Form;

/**
 * @author Stepan Anchugov <kixxx1@gmail.com>
 */
abstract class AbstractType
{

    /**
     * @var array
     */
    private $fields = array();

    /**
     * Func describes and adds fields to self
     *
     * @abstract
     * @return mixed
     */
    abstract public function buildForm();

    /**
     * @return array
     */
    public function getFields()
    {
        return $this->fields;
    }

}
