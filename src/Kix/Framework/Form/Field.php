<?php
namespace Kix\Framework\Form;

/**
 * @author Stepan Anchugov <kixxx1@gmail.com>
 */
class Field
{

    /**
     * @var
     */
    public $type;

    /**
     * @var
     */
    public $value;

    /**
     * @var Callable[]
     */
    private $rules;

    public function __construct(array $params)
    {
        $this->type =  $params['type'];
        $this->value = $params['value'];

        foreach ($params['rules'] as $className) {
            // quick and dirty
            $this->rules[] = $className.'::validate';
        }
    }

    public function validate()
    {
        $result = true;

        foreach ($this->rules as $rule) {
            $result = $result && call_user_func($rule, $this->value);
        }

        return $result;
    }

    public function render()
    {
        switch ($this->type) {
            case 'text':
                return '<input type="text" value="'.$this->value.'">';
                break;
            case 'date':
                break;
            // ...
        }
    }

}
