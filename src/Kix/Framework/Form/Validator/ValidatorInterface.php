<?php
namespace Kix\Framework\Form\Validator;

/**
 * @author Stepan Anchugov <kixxx1@gmail.com>
 */
interface ValidatorInterface
{

    /**
     * @abstract
     * @param $value
     * @return bool
     */
    public static function validate($value);

}
