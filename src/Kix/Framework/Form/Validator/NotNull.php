<?php
namespace Kix\Framework\Form\Validator;

/**
 * @author Stepan Anchugov <kixxx1@gmail.com>
 */
class NotNull implements ValidatorInterface
{

    public static function validate($value)
    {
        return (bool) $value;
    }

}
