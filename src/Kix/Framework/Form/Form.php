<?php
namespace Kix\Framework\Form;

/**
 * @author Stepan Anchugov <kixxx1@gmail.com>
 */
class Form
{

    /**
     * @var array
     */
    private $fields = array();

    public static function create(AbstractType $type)
    {
        $form = new self();

        foreach ($type->getFields() as $fieldDef) {
            $form->addField(new Field($fieldDef));
        }

        return $form;
    }

    private function addField($field)
    {
        $this->fields [] = $field;
    }

    public function validate()
    {
        $result = true;

        foreach ($this->fields as $field) {
            $result = $result && $field->validate();
        }

        return $result;
    }

    protected function render()
    {
        $result = '';

        foreach ($this->fields as $field) {
            $result .= $field->render();
        }

        return $result;
    }

}
