<?php
namespace Kix\Framework;

use Kix\Framework\DI\ContainerAware;

/**
 * @author Stepan Anchugov <kixxx1@gmail.com>
 */
class Router implements ContainerAware
{

    /**
     * @var Router\RouteCollection[]
     */
    private $_routeCollections = array();

    /**
     * @var DI\Container
     */
    private $_container;

    public function match($uri)
    {
        foreach ($this->_routeCollections as $packageName => $collection) {
            foreach ($collection->getRoutes() as $route) {
                if ($params = $route->match($uri)) {
                    $target = $route->splitTarget();
                    $target['package'] = $packageName;
                    $target['params']  = $this->filterParams($params);

                    return $target;
                }
            }
        }
    }

    private function filterParams($paramList)
    {
        $return = array();

        foreach ($paramList as $k => $v) {
            if (is_string($k)) {
                $return[$k] = $v;
            }
        }

        return $return;
    }

    /**
     * @param $key
     * @param Router\RouteCollection $collection
     */
    public function addRouteCollection($key, Router\RouteCollection $collection)
    {
        $this->_routeCollections[$key] = $collection;
    }

    public function setContainer($container)
    {
        if ($container instanceof DI\Container) {
            $this->_container = $container;
        }
    }

}
