<?php
namespace Kix\Framework;

use Kix\Framework\DI\ContainerAware;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * @author Stepan Anchugov <kixxx1@gmail.com>
 */
class Controller
{

    /**
     * @var Kix\Framework\DI\Container
     */
    protected $_container;

    public function setContainer($container)
    {
        $this->_container = $container;
    }

    /**
     * @return \Doctrine\ORM\EntityManger
     */
    public function getEm()
    {
        return $this->_container->get('em');
    }

    /**
     * @return \Twig_Environment
     */
    public function getTwig()
    {
        return $this->_container->get('twig');
    }

    /**
     * @return \Symfony\Component\HttpFoundation\Request
     */
    public function getRequest()
    {
        return $this->_container->get('request');
    }

    /**
     * A shortcut
     *
     * @param $template
     * @param array $params
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function render($template, array $params = array())
    {
        return new Response($this->getTwig()->render($template, $params));
    }

    public function redirect($target, $status = 302)
    {
        return new RedirectResponse($target, $status);
    }

}
