<?php
namespace Kix\Framework;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

use Kix\Framework\DI\ContainerAware;

/**
 * Ядро приложения
 *
 * @author Stepan Anchugov <kixxx1@gmail.com>
 */
abstract class Kernel
{

    /**
     * @var array
     */
    private $_packages;

    /**
     * @var DI\Container
     */
    private $_container;

    /**
     * Собираем все пэкеджи, регистрируем роуты из них
     */
    public function __construct()
    {
        $this->_container = new DI\Container();
        $this->_container->set('router', new Router());
    }

    protected function addPackage(Package $package)
    {
        $this->_packages []= $package;

        $this->_container->get('router')->addRouteCollection(get_class($package), $package->getRoutes());
        $this->_container->get('twig')->getLoader()->addPath($package->getTemplatePath());
    }

    /**
     * В наследнике описываются пакеты, которые мы собираемся подключать
     * @return mixed
     */
    abstract public function initPackages();

    /**
     * @param Request $request
     * @return Response
     */
    public function handle($request)
    {
        $target = $this->_container->get('router')->match($request->getRequestUri());

        if (!$target) {
            throw Exception::routeUnmatched($request->getRequestUri());
        }

        $package = new $target['package']();
        $namespace = $package->getNamespace();

        $className = $namespace . '\\Controller\\' . $target['controller'];
        $controller = new $className();

        $this->_container->set('request', $request);

        if ($controller instanceof ContainerAware) {
            $controller->setContainer($this->_container);
        }

        if (method_exists($controller, $target['action'])) {
            $response = call_user_func_array(array($controller, $target['action']), $target['params']);
        } else {
            throw Exception::actionDoesNotExist($target['action'], $target['controller']);
        }

        if (!$response instanceof Response) {
            throw Exception::noResponseGiven($target['action'], $target['controller']);
        } else {
            return $response;
        }

    }

    public function getContainer()
    {
        return $this->_container;
    }

}
