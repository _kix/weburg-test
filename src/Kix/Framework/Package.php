<?php
namespace Kix\Framework;

/**
 * @author Stepan Anchugov <kixxx1@gmail.com>
 */
abstract class Package
{

    /**
     * @return string
     */
    public function getPath()
    {
        $reflected = new \ReflectionObject($this);
        return dirname($reflected->getFileName());
    }

    /**
     * @return string
     */
    public function getClassname()
    {
        return get_class($this);
    }

    /**
     * Dirty
     * @return string
     */
    public function getNamespace()
    {
        $pos = strrpos($this->getClassname(), '\\');
        return substr($this->getClassname(), 0, $pos);
    }

    /**
     * In ideal world this should be done via DI without hardcoded strings
     * And Collection should be global
     */
    public function getRoutes()
    {
        $collection = new Router\RouteCollection();
        $ret = array();

        $routesConfig = realpath($this->getPath().'/Resources/config/routes.php');
        if ($routesConfig) {
            $routes = require($routesConfig);
            if (is_array($routes)) {
                /**
                 * Supported notation:
                 * 'route_name' => [pattern, target]
                 * 'pattern'    => target
                 */
                foreach ($routes as $k => $v) {
                    $route = new Router\Route($k, $v);
                    $ret []= $route;
                    $collection->addRoute(new Router\Route($k, $v), $this);
                }
            }
        }

        return $collection;
    }

    public function getTemplatePath()
    {
        $reflected = new \ReflectionObject($this);
        return dirname($reflected->getFileName()) . '/Resources/views';
    }

}
