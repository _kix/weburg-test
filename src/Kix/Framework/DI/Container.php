<?php
namespace Kix\Framework\DI;

/**
 * Stupid DI
 * @author Stepan Anchugov <kixxx1@gmail.com>
 */
class Container
{

    /**
     * @var array
     */
    private $content;

    public function get($key)
    {
        if ($this->content instanceof ContainerAware) {
            $this->content[$key]->setContainer($this);
        }
        if (!isset($this->content[$key])) {
            throw new \Exception(sprintf('Key %s is not set in the container', $key));
        }
        return $this->content[$key];
    }

    public function set($key, $value)
    {
        if (!isset($this->content[$key])) {
            $this->content[$key] = $value;
        } else {
            throw new \Exception(sprintf('Id %s is already taken in this container', $key));
        }
    }

}
