<?php
namespace Kix\Framework\DI;

/**
 * @author Stepan Anchugov <kixxx1@gmail.com>
 */
interface ContainerAware
{

    public function setContainer($container);

}
