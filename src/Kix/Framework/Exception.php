<?php
namespace Kix\Framework;

/**
 * @author Stepan Anchugov <kixxx1@gmail.com>
 */
class Exception extends \Exception
{

    public static function actionDoesNotExist($action, $controller)
    {
        return new self(sprintf('Action %s does not exist in controller %s', $action, $controller));
    }

    public static function noResponseGiven($action, $controller)
    {
        return new self(sprintf('Action %s in controller %s did not return a response', $action, $controller));
    }

    public static function routeUnmatched($uri)
    {
        return new self(sprintf('Requested URI `%s` did not match any of defined routes', $uri));
    }
}
