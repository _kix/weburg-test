<?php
/**
 * @author Stepan Anchugov <kixxx1@gmail.com>
 */
return array(

    '/^\/$/'                                        => 'DefaultController::indexAction',

    '/^\/genres\/list\/$/'                          => 'GenresController::listAction',
    '/^\/genres\/add\/$/'                           => 'GenresController::addAction',
    '/^\/genres\/delete\/(?P<id>\d+)\/$/'           => 'GenresController::deleteAction',

    '/^\/units\/list\/$/'                           => 'UnitsController::listAction',
    '/^\/units\/add\/$/'                            => 'UnitsController::addAction',
    '/^\/units\/view\/(?P<id>\d+)\/$/'              => 'UnitsController::viewAction',
    '/^\/units\/edit\/(?P<id>\d+)\/$/'              => 'UnitsController::editAction',
    '/^\/units\/delete\/(?P<id>\d+)\/$/'            => 'UnitsController::deleteAction',

);