<?php
namespace Kix\MovieDb\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

use Kix\Framework\DI\ContainerAware;
use Kix\Framework\Controller;

/**
 * @author Stepan Anchugov <kixxx1@gmail.com>
 */
class DefaultController extends Controller
                        implements ContainerAware
{

    public function indexAction()
    {
        /** @var $twig \Twig_Environment */
        $twig = $this->getTwig();

        return new Response(
            $twig->render('index.html.twig', array())
        );
    }

    public function removeAction($id)
    {
        var_dump($this->_container->get('em')); die;
    }

    public function addUserAction()
    {
        $em = $this->getEm();

        $user = new \Kix\MovieDb\Entities\User();
        $user->setName('Tester');

        $em->persist($user);
        $em->flush();
    }

    public function listUsersAction()
    {
        $em = $this->getEm();
        $twig = $this->getTwig();

        $repo = $em->getRepository('\Kix\MovieDb\Entities\User');

        $users = $repo->findAll();

        return new Response(
            $twig->render('list-users.html.twig', array(
            'users' => $users,
            ))
        );
    }

}
