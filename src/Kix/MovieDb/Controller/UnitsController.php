<?php
namespace Kix\MovieDb\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

use Kix\Framework\DI\ContainerAware;
use Kix\Framework\Controller;

use Kix\MovieDb\Entities;

/**
 * @author Stepan Anchugov <kixxx1@gmail.com>
 */
class UnitsController extends Controller
                      implements ContainerAware
{

    public function listAction()
    {
        $repo = $this->getEm()->getRepository('\Kix\MovieDb\Entities\VideoUnit');
        $units = $repo->findAll();

        return $this->render('listUnits.html.twig', array(
            'units' => $units,
        ));
    }

    public function viewAction($id)
    {
        $repo = $this->getEm()->getRepository('\Kix\MovieDb\Entities\VideoUnit');
        $unit = $repo->find($id);

        if ($unit instanceof Entities\VideoUnit) {
            return $this->render('viewUnit.html.twig', array(
                'unit' => $unit,
            ));
        } else {
            return $this->redirect('/404');
        }

    }

    public function addAction()
    {
        $req = $this->getRequest();

        if ($req->getMethod() == 'GET') {
            $repo = $this->getEm()->getRepository('\Kix\MovieDb\Entities\Genre');
            $genres = $repo->findAll();

            return $this->render('addUnit.html.twig', array(
                'genres' => $genres,
            ));
        } elseif ($req->getMethod() == 'POST') {
            $unit = new Entities\VideoUnit();

            // @todo: validation and form generation are out of the picture
            // can't do that in 8 hours, yeah

            $unit->setTitle($req->request->get('unit_title'));
            $unit->setCountry($req->request->get('unit_country'));
            $unit->setFilmStartYear($req->request->get('unit_film_start'));
            $unit->setFilmEndYear($req->request->get('unit_film_end'));
            $unit->setDirector($req->request->get('unit_director'));
            $unit->setActors(explode(',', $req->request->get('unit_actors')));
            $unit->setLength($req->request->get('unit_length'));
            $unit->setPremiereDate($req->request->get('unit_premiere'));
            $unit->setAnnounce($req->request->get('unit_announce'));
            $unit->setDescription($req->request->get('unit_description'));

            $this->getEm()->persist($unit);

            foreach ((array) $req->request->get('unit_genres') as $genreId) {
                $relation = new Entities\GenreUnitRelation();
                $relation->setGenre($genreId);
                $relation->setVideoUnit($unit->getId());
            }

            $this->getEm()->persist($relation);

            $this->getEm()->flush();

            return $this->redirect('/units/list/');
        }
    }

    public function editAction($id)
    {
        return $this->render('editUnit.html.twig');
    }

    public function deleteAction($id)
    {
        $unit = $this->getEm()->getRepository('Kix\MovieDb\Entities\VideoUnit')->find($id);

        if ($unit instanceof Entities\VideoUnit) {

            $this->getEm()->remove($unit);
            $this->getEm()->flush();

            return $this->redirect('/units/list/');
        } else {
            return $this->redirect('/404');
        }
    }

}
