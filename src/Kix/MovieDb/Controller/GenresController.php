<?php
namespace Kix\MovieDb\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

use Kix\Framework\DI\ContainerAware;
use Kix\Framework\Controller;

use Kix\MovieDb\Entities;

/**
 * @author Stepan Anchugov <kixxx1@gmail.com>
 */
class GenresController extends Controller
                       implements ContainerAware
{

    public function listAction()
    {
        $repo = $this->getEm()->getRepository('\Kix\MovieDb\Entities\Genre');
        $genres = $repo->findAll();

        return $this->render('listGenres.html.twig', array(
            'genres' => $genres,
        ));
    }

    public function addAction()
    {
        $req = $this->getRequest();
        if ($req->getMethod() == 'GET') {
            return $this->render('addGenre.html.twig');
        } elseif ($req->getMethod() == 'POST') {
            $genre = new Entities\Genre();

            $genre->setName($req->request->get('genre_name'));
            $this->getEm()->persist($genre);
            $this->getEm()->flush();

            return $this->redirect('/genres/list/');
        }
    }

    public function deleteAction($id)
    {
        $genre = $this->getEm()->getRepository('\Kix\MovieDb\Entities\Genre')->find($id);

        if ($genre instanceof Entities\Genre) {
            $this->getEm()->remove($genre);
            $this->getEm()->flush();

            return $this->redirect('/genres/list/');
        } else {
            return $this->redirect('/404');
        }
    }


}
