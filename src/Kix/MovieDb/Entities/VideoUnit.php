<?php
namespace Kix\MovieDb\Entities;

/**
 * @author Stepan Anchugov <kixxx1@gmail.com>
 * @Entity
 * @Table(name="videoUnits")
 */
class VideoUnit
{

    /**
     * @Id
     * @GeneratedValue
     * @Column(type="integer")
     * @var integer
     */
    protected $id;

    /**
     * @var
     * @Column()
     */
    protected $title;

    /**
     * @var
     * @Column
     */
    protected $country;

    /**
     * @var
     * @Column(type="smallint")
     */
    protected $filmStartYear;

    /**
     * @var
     * @Column(type="smallint")
     */
    protected $filmEndYear;

    /**
     * @var string
     * @Column
     */
    protected $director;

    /**
     * @var
     * @Column(type="object")
     */
    protected $actors;

    /**
     * @var
     * @Column(type="integer")
     */
    protected $length;

    /**
     * @var
     * @Column()
     */
    protected $premiereDate;

    /**
     * @var
     * @Column(type="text")
     */
    protected $announce;

    /**
     * @var
     * @Column(type="text")
     */
    protected $description;

    /**
     * @ORM\ManyToMany(targetEntity="Genre", inversedBy="video_units")
     * @ORM\JoinTable(name="units_genres",
     *      joinColumns={@ORM\JoinColumn(name="unit_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="genre_id", referencedColumnName="id")}
     * )
     */
    protected $genres;

    public function __construct()
    {
        $this->genres = new \Doctrine\Common\Collections\ArrayCollection();
    }

    public function getGenres()
    {
        return $this->genres;
    }

    public function getId()
    {
        return $this->id;
    }

    public function setTitle($title)
    {
        $this->title = $title;
    }

    public function getTitle()
    {
        return $this->title;
    }

    public function setCountry($country)
    {
        $this->country = $country;
    }

    public function getCountry()
    {
        return $this->country;
    }

    public function setFilmStartYear($year)
    {
        $this->filmStartYear = $year;
    }

    public function getFilmStartYear()
    {
        return $this->filmStartYear;
    }

    public function setFilmEndYear($year)
    {
        $this->filmEndYear = $year;
    }

    public function getFilmEndYear()
    {
        return $this->filmEndYear;
    }

    public function getDirector()
    {
        return $this->director;
    }

    public function setDirector($director)
    {
        $this->director = $director;
    }

    public function setActors(array $actors)
    {
        $this->actors = $actors;
    }

    public function getActors()
    {
        return $this->actors;
    }

    public function setLength($length)
    {
        $this->length = $length;
    }

    public function getLength()
    {
        return $this->length;
    }

    public function setPremiereDate($date)
    {
        $this->premiereDate = $date;
    }

    public function getPremiereDate()
    {
        return $this->premiereDate;
    }

    public function setAnnounce($announce)
    {
        $this->announce = $announce;
    }

    public function getAnnounce()
    {
        return $this->announce;
    }

    public function setDescription($desc)
    {
        $this->description = $desc;
    }

    public function getDescription()
    {
        return $this->description;
    }

}
