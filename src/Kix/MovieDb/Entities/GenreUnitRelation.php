<?php
namespace Kix\MovieDb\Entities;

/**
 * @author Stepan Anchugov <kixxx1@gmail.com>
 * @Entity
 * @Table(name="genres_units")
 */
class GenreUnitRelation
{

    /**
     * @Column(type="integer")
     * @Id
     * @GeneratedValue
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="Genre", cascade={"persist"})
     * @ORM\JoinColumn(name="genre_id", referencedColumnName="id")
     */
    protected $genre;

    /**
     * @ORM\ManyToOne(targetEntity="VideoUnit", cascade={"persist"})
     * @ORM\JoinColumn(name="unit_id", referencedColumnName="id")
     */
    protected $videoUnit;

    public function setGenre($genre)
    {
        $this->genre = $genre;
    }

    public function setVideoUnit($unit)
    {
        $this->videoUnit = $unit;
    }

}
