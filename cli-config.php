<?php
/**
 * @author Stepan Anchugov <kixxx1@gmail.com>
 */
require_once "app/bootstrap.php";
$em = require_once "app/doctrine.php";

$helperSet = new \Symfony\Component\Console\Helper\HelperSet(array(
    'em' => new \Doctrine\ORM\Tools\Console\Helper\EntityManagerHelper($em)
));