Минимальная версия PHP - 5.3.6.

Зависимости в проекте управляются при помощи Composer. Он устанавиливается вот так:

    curl -s https://getcomposer.org/installer | php

Потом надо установить зависимости:

    php composer.phar install

Теперь проект заработает, если скормить в Nginx примерно такой конфиг:

    server {
      listen 80;

      server_name wbrg.kix.dev;
      root /path/to/project/web;

      # strip app.php/ prefix if it is present
      rewrite ^/app\.php/?(.*)$ /$1 permanent;

      location / {
        index app.php;
        if (-f $request_filename) {
          break;
        }
        rewrite ^(.*)$ /app.php/$1 last;
      }

      # pass the PHP scripts to FastCGI server listening on 127.0.0.1:9000
      location ~ ^/(app|app_dev|app_test)\.php(/|$) {
        fastcgi_pass   127.0.0.1:9001;
        fastcgi_split_path_info ^(.+\.php)(/.*)$;
        fastcgi_index app.php;
        include fastcgi_params;
        fastcgi_param  SCRIPT_FILENAME    $document_root$fastcgi_script_name;
        fastcgi_param  HTTPS              off;
      }
    }

