<?php
/**
 *
 */

require_once('../vendor/autoload.php');
require_once('../app/AppKernel.php');

use Symfony\Component\HttpFoundation\Request;

define('APP_ROOT', realpath(__DIR__.'/../'));

if (!defined('APP_ENV')) {
    define('APP_ENV', 'dev');
}

$kernel = new AppKernel();

$autoload = new Twig_Autoloader();
$autoload::register();

$loader = new Twig_Loader_Filesystem(array());
$twig   = new Twig_Environment($loader);
$kernel->getContainer()->set('twig', $twig);

$entityManager = require_once('../app/doctrine.php');
$kernel->getContainer()->set('em', $entityManager);

$kernel->initPackages();
$kernel->handle(Request::createFromGlobals())->send();