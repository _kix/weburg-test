<?php
/**
 * @author Stepan Anchugov <kixxx1@gmail.com>
 */

use Doctrine\ORM\EntityManager,
    Doctrine\ORM\Configuration;

//Setup::registerAutoloadPEAR();

$cache = new \Doctrine\Common\Cache\ArrayCache;

$config = new Configuration;
$config->setMetadataCacheImpl($cache);
$driverImpl = $config->newDefaultAnnotationDriver('src/Kix/MovieDb/Entities');

$config->setMetadataDriverImpl($driverImpl);
$config->setQueryCacheImpl($cache);
$config->setProxyDir('app/cache/Proxies');
$config->setProxyNamespace('Proxies');

$config->setAutoGenerateProxyClasses(true);

$connectionOptions = array(
    'driver' => 'pdo_sqlite',
    'path' => __DIR__.'/database.sqlite'
);

$entityManager = EntityManager::create($connectionOptions, $config);
return $entityManager;