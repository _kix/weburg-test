<?php
use Kix\Framework\Kernel;

/**
 * @author Stepan Anchugov <kixxx1@gmail.com>
 */
class AppKernel extends Kernel
{

    public function initPackages()
    {
        $this->addPackage(new \Kix\MovieDb\MovieDb());
    }

}
